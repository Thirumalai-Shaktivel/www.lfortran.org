---
title: "Fortran is fast"
icon: "fa fa-fighter-jet"
---
Fortran is built from the ground up to translate mathematics into simple,
readable, and fast code.
